import { has, cloneDeepWith, omit } from 'lodash';

/**
 * Clone object comming from gql and remove GQL specific fields
 *
 * @param obj Object returned from GQL query
 */
export const cleanGql = <T>(obj: T): T => {
	const customizer = (obj: any): any => {
		if (has(obj, "__typename")) {
			return cloneDeepWith(omit(obj, "__typename"), customizer);
		}
	}

	return cloneDeepWith(obj, customizer);
}
