import { DocumentNode } from "@apollo/client";
import { OperationDefinitionNode } from "graphql";

export function getQueryName(query: DocumentNode) {
	return (query.definitions[0] as OperationDefinitionNode).name!.value;
}
